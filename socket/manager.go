package socket

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type Manager struct {
	Clients map[string]*Client
}

type Message struct {
	Sender  string `json:"sender"`
	Content string `json:"content"`
}

func NewManager() *Manager {
	return &Manager{
		Clients: make(map[string]*Client)}
}

func (manager *Manager) register(client *Client) {
	manager.Clients[client.ID] = client
}

func (manager *Manager) unregister(client *Client) {
	delete(manager.Clients, client.ID)
}

func (manager *Manager) Serve(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
		return
	}
	client := &Client{ID: uuid.New().String(), conn: conn, manager: manager, writeDone: make(chan bool)}
	manager.register(client)
	log.Println("Client created. Client ID: " + client.ID)
	client.Read() // read from the socket once and exit
}

func (manager *Manager) Write(w http.ResponseWriter, r *http.Request) {
	var response CallbackMessage
	json.NewDecoder(r.Body).Decode(&response)

	manager.Clients[response.ClientID].Write(response.Message) // Reuse client connection and write message
}
