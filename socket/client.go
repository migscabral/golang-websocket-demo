package socket

import (
	"log"

	"github.com/gorilla/websocket"
)

type Client struct {
	ID        string
	conn      *websocket.Conn
	manager   *Manager
	writeDone chan bool
}

type CallbackMessage struct {
	ClientID string  `json:"clientID"`
	Message  Message `json:"message"`
}

func (client *Client) Write(message Message) {
	err := client.conn.WriteJSON(message)
	if err != nil {
		log.Fatal(err)
		client.conn.Close()
	}
	log.Println("Message written: ", message)
	log.Println("After write: ", &(client.conn))

	client.writeDone <- true
}

func (client *Client) Read() Message {
	defer func() {
		client.manager.unregister(client)
		client.conn.Close()
		log.Println("Connection closed. Client ID: " + client.ID)
	}()

	// Some long running process here. Long running process calls callback url which should send true to writeDone channel

	message := Message{}
	select {
	case <-client.writeDone: // Block here wait for writeDone
		log.Println("Before read: ", &(client.conn))
		err := client.conn.ReadJSON(&message)
		log.Println("After read")
		if err != nil {
			log.Fatal(err)
			client.conn.Close()
			break
		}
		log.Println("Message received: ", message)
	}
	return message
}
