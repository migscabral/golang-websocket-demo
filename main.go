package main

import (
	"log"
	"net/http"
	_ "net/http/pprof"
	"runtime"
	"strconv"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"org.amihan/demo/websocket/handler"
)

type app struct {
	wsHandler *handler.WSHandler
}

func main() {
	port := "8080"
	app := app{
		wsHandler: handler.NewWSHandler()}
	r := mux.NewRouter()
	r.HandleFunc("/ws", app.wsHandler.Serve)
	r.HandleFunc("/ws/callback", app.wsHandler.Callback).Methods("POST")

	r.HandleFunc("/countgoroutines", func(w http.ResponseWriter, r *http.Request) {
		count := runtime.NumGoroutine()
		w.Write([]byte(strconv.Itoa(count)))
	}).Methods("GET")
	r.PathPrefix("/debug/pprof/").Handler(http.DefaultServeMux)
	http.Handle("/", r)

	log.Println("Server started on ", port)

	log.Fatal(http.ListenAndServe(":"+port, handlers.CORS()(r)))
}
