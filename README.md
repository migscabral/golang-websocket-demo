# golang-websocket-demo

1. Build and run:
```
$ go build -mod vendor && go run -mod vendor main.go 
```

2. On a separate shell. This will generate a client ID.
```
$ websocat ws://localhost:8080/ws
```


3. Yet on another shell:
```
$ curl -d '{"clientID": "<get from created client ID by websocat, see in go terminal stdout","message": {"sender": "Bob","content": "Hello world!"}}' -H "Content-Type: application/json" -X POST http://localhost:8080/ws/callback
```