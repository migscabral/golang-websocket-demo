module org.amihan/demo/websocket

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/rs/cors v1.7.0 // indirect
	go.uber.org/zap v1.14.1
)
