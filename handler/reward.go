package handler

import (
	"net/http"

	"org.amihan/demo/websocket/socket"
)

type WSHandler struct {
	manager *socket.Manager
}

func NewWSHandler() *WSHandler {
	return &WSHandler{
		manager: socket.NewManager()}
}

func (wsh *WSHandler) Serve(res http.ResponseWriter, req *http.Request) {
	wsh.manager.Serve(res, req)
}

func (wsh *WSHandler) Callback(res http.ResponseWriter, req *http.Request) {
	wsh.manager.Write(res, req)
}
